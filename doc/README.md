O programa deve ser executado num sistema Linux ou Mac, pois utiliza system("read").
Caso seja executado em Windows, mudar os **system("read")** para **system("pause")**.

Para compilar o programa:

`make`

Para executar o programa:

`make run`

Para limpar os arquivos .o e bin do programa:

`make clean`