#include "circulo.hpp"
#include <iostream>

Circulo::Circulo(){

    pi = 3.141592;
    set_tipo("Circulo");
    set_base(3);
}

Circulo::Circulo(float base){
    // Base é igual ao raio da circunferencia.
    pi = 3.141592;
    set_tipo("Circulo");
    set_base(base);
    set_altura(base);

}

Circulo::~Circulo(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}

float Circulo::get_pi(){
    return pi;
}

float Circulo::calcula_area(){
    return pi*get_base()*get_base();
}

float Circulo::calcula_perimetro(){
    return get_base()*pi*2;
}