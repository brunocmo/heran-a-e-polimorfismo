#include "paralelogramo.hpp"
#include <iostream>

Paralelogramo::Paralelogramo(){
        set_tipo("Paralelogramo");
}

Paralelogramo::Paralelogramo(string tipo, float base, float altura){
        set_tipo(tipo);
        set_base(base);
        set_altura(altura);
}

Paralelogramo::~Paralelogramo(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}

float Paralelogramo::calcula_perimetro(){
    return 2*(get_base()+get_altura());
}