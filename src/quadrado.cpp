#include "quadrado.hpp"
#include <iostream>
using namespace std;
Quadrado::Quadrado(){
}

Quadrado::Quadrado(string tipo, float base, float altura){
    set_tipo(tipo);
    set_base(base);
    set_altura(base);
}

Quadrado::~Quadrado(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}

float Quadrado::calcula_area(){
    return get_base()*get_base();
}

float Quadrado::calcula_perimetro(){
    return 4*get_base();
}

