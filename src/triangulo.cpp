#include "triangulo.hpp"
#include <iostream>

Triangulo::Triangulo(){

	set_tipo("Triangulo");
	set_base(10.0);
	set_altura(5.0);
}

Triangulo::Triangulo(float base, float altura){

    set_tipo("Triangulo");
	set_base(base);
	set_altura(altura);
}

Triangulo::Triangulo(string tipo, float base, float altura){

	set_tipo(tipo);
	set_base(base);
	set_altura(altura);
}

Triangulo::~Triangulo(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}

float Triangulo::calcula_area(){
	
	return (get_base()*get_altura())/2;
}

float Triangulo::calcula_perimetro(){
	return get_base()+get_altura();
}
