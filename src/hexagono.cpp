#include "hexagono.hpp"
#include <iostream>

Hexagono::Hexagono(){
    set_tipo("Hexagono");
    set_base(0);
    set_altura(0);
}

Hexagono::Hexagono(float base, float altura){
    set_tipo("Hexagono");
    set_base(base);
    set_altura(altura);
}

Hexagono::~Hexagono(){
    cout << "Destruindo o objeto: " << get_tipo() << endl;
}

float Hexagono::calcula_area(){
    return 6*((get_base()*get_altura())/2);
}

float Hexagono::calcula_perimetro(){
    return 6*get_base();
}

float Hexagono::calculaalgo(){

    return 2*get_base();
}

float Hexagono::calculaalgo(float algo){
    return 2*algo;
}