/* Universidade de Brasilia - Faculade do Gama
 * Orientacao a objetos - Turma B
 * Bruno Carmo Nunes - 18/0117548
 * Exerc�cio de Heranca e polimorfismo
 */

#include <iostream>
#include <vector>
#include "formageometrica.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

int main (int argc, char* argv[]){

	vector<FormaGeometrica * > formas;

	system("clear");

	string tipo;
    float base;
    float altura;
    int opcao;
    int opcao2;


    base = 4;
    altura = 5;

    formas.push_back(new Quadrado("Quadrado",base,altura));

    base = 2;
    altura = 7;

    formas.push_back(new Paralelogramo("Paralelogramo",base,altura));

    base = 3;
    altura = 6;

    formas.push_back(new Triangulo(base,altura));

    base = 5;
    altura = 5;

    formas.push_back(new Circulo(base));

    base = 6;
    altura = 8;

    formas.push_back(new Hexagono(base,altura));

    base = 9;
    altura = 10;

    formas.push_back(new Pentagono("Pentagono",base,altura));

    do {

	    cout << "==================================" << endl;
	    cout << "\t Formas Geometricas \t" << endl;
        cout << "==================================" << endl;

        cout << "1. Mostrar lista de formas geometricas" << endl;
        cout << "2. Adicionar forma geometrica" << endl;
        cout << "0. Sair" << endl;
        cout << "==================================" << endl;

        cout << "Selecione uma opcao: ";
        cin >> opcao;

        putchar('\n');

        system("clear");

        if(opcao == 1){

            for(FormaGeometrica * f: formas ){
                cout << "Tipo: " << f->get_tipo() << endl;
                cout << "Base: " << f->get_base() << endl;
                cout << "Altura: " << f->get_altura() << endl;
                cout << "Area: " << f->calcula_area() << endl;
                cout << "Perimetro: " << f->calcula_perimetro() << endl << endl;
            }

            cout << "Pressione ENTER para continuar..." << endl;
            system("read");
            system("clear");

        }

        if(opcao == 2){

            system("clear");

            cout << "==================================" << endl;
            cout << "\t Formas Geometricas \t" << endl;
            cout << "==================================" << endl;

            cout << "1. Adicionar triangulo" << endl;
            cout << "2. Adicionar quadrado" << endl;
            cout << "3. Adicionar pentagono" << endl;
            cout << "4. Adicionar hexagono" << endl;
            cout << "5. Adicionar circulo" << endl;
            cout << "6. Adicionar paralelogramo" << endl;
            cout << "0. Voltar" << endl;
            cout << "==================================" << endl;

            cin >> opcao2;

            switch (opcao2){

                case 1 :
                    cout << "Digite a base: ";
                    cin >> base;
                    cout << "Digite a altura: ";
                    cin >> altura;
                    formas.push_back(new Triangulo(base,altura));
                    system("clear");
                    break;
                case 2 :
                    cout << "Digite a base: ";
                    cin >> base;
                    altura = base;
                    formas.push_back(new Quadrado("Quadrado",base,altura));
                    system("clear");
                    break;
                case 3 :
                    cout << "Digite a base: ";
                    cin >> base;
                    cout << "Digite a altura: ";
                    cin >> altura;
                    formas.push_back(new Pentagono("Pentagono",base,altura));
                    system("clear");
                    break;
                case 4 :
                    cout << "Digite a base: ";
                    cin >> base;
                    cout << "Digite a altura: ";
                    cin >> altura;
                    formas.push_back(new Hexagono(base,altura));
                    system("clear");
                    break;
                case 5 :
                    cout << "Digite a base(raio): ";
                    cin >> base;
                    altura = base;
                    formas.push_back(new Circulo(base));
                    system("clear");
                    break;
                case 6 :
                    cout << "Digite a base: ";
                    cin >> base;
                    cout << "Digite a altura: ";
                    cin >> altura;
                    formas.push_back(new Paralelogramo("Paralelogramo",base,altura));
                    system("clear");
                    break;

                default:
                    system("clear");
                    break;

            }

        }

        if(opcao > 7 || opcao < 0){

            cout << "Opcao invalida!" << endl;
            cout << "Pressione ENTER para continuar..." << endl;
            system("read");
            system("clear");

        }

	}while(opcao != 0);

	return 0;
}
