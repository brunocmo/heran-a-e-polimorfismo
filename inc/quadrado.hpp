#ifndef QUADRADO_HPP
#define QUADRARO_HPP

#include "formageometrica.hpp"

class Quadrado: public FormaGeometrica{

public:
        Quadrado();
        Quadrado(string tipo, float base, float altura);
        ~Quadrado();

        float calcula_area();
        float calcula_perimetro();

};

#endif
