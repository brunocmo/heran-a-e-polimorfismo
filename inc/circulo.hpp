#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formageometrica.hpp"

class Circulo: public FormaGeometrica{

private:
        float pi;
public:
        Circulo();
        Circulo(float base);
        ~Circulo();

        float get_pi();
        float calcula_area();
        float calcula_perimetro();

};

#endif