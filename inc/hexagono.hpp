#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP
#include "formageometrica.hpp"

class Hexagono: public FormaGeometrica{

public:
    Hexagono();
    Hexagono(float base, float altura);
    ~Hexagono();

    float calcula_area();
    float calcula_perimetro();

    float calculaalgo();
    float calculaalgo(float algo);


};


#endif