#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP
#include "formageometrica.hpp"

class Paralelogramo: public FormaGeometrica{

public:
    Paralelogramo();
    Paralelogramo(string tipo, float base, float altura);
    ~Paralelogramo();

    float calcula_perimetro();

};

#endif